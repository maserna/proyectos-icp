/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import entidades.Proyecto;
import java.io.Serializable;
import java.util.Collection;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.ProyectoFachada;

/**
 * Controla la transferencia de información desde las paginas busca.xhtml y
 * resultadoBusqueda.xhtml hacia ProyectoFachada para realizar búsquedas por
 * titulo, titulo y tags, tags
 *
 * @author Grupo DCJ
 * @version 20-02-2014
 */
@ManagedBean(name = "controladorProyecto")
@SessionScoped
public class ProyectoControlador implements Serializable {

    private Proyecto proyecto;
    @EJB
    private ProyectoFachada proyectoFachada;
    private String busqueda;
    private String opcionesBusqueda;
    private String opcionesEstudio;
    private String fechaInicial;
    private String fechafinal;

    public ProyectoControlador() {
    }

    public String getFechaInicial() {
        return fechaInicial;
    }

    public String getFechafinal() {
        return fechafinal;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public void setFechafinal(String fechafinal) {
        this.fechafinal = fechafinal;
    }

    public void setOpcionesEstudio(String opcionesEstudio) {
        this.opcionesEstudio = opcionesEstudio;
    }

    public String getOpcionesEstudio() {
        return opcionesEstudio;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public String getOpcionesBusqueda() {
        return opcionesBusqueda;
    }

    public void setOpcionesBusqueda(String opcionesBusqueda) {
        this.opcionesBusqueda = opcionesBusqueda;
    }

    /**
     * parametriza los valores de los atributos con el fin de parametrizar la
     * búsqueda del usuario
     *
     * @return void
     */
    public void inicioOpciones() {
        opcionesBusqueda = "1";
        busqueda = "";
        opcionesEstudio = "0";
        fechaInicial = "";
        fechafinal = "";
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    /*ejecuta un llamado a resultadoBusqueda para mostrar al usuario el resulta de una búsqueda por el criterio que 
     * haya seleccionado
     * @return String  "resultadoBusqueda" llamado a resultadoBusqueda.xhtml
     */
    public String resultadoBusqueda() {
        return "resultadoBusqueda";
    }

    public Proyecto getProyecto() {
        if (proyecto == null) {
            proyecto = new Proyecto();
        }
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * prepara la búsqueda por cada uno de los criterios que el usuario
     * seleccione a través de las vistas
     *
     * @return Collection<Proyecto> retorna una coleccion de proyectos que
     * coinciden con
     */
    public Collection<Proyecto> getPrepararBusqueda() {
        String condicion = "";
        if (opcionesBusqueda.equals("4")) {
            return proyectoFachada.buscarCodigo(busqueda);
        } else {
            if (!fechaInicial.equals("") && !fechafinal.equals("")) {
                condicion = " AND año > " + fechaInicial + " AND año < " + fechafinal + "";
            } else if (!fechaInicial.equals("")) {
                condicion = " AND año > " + fechaInicial + "";
            } else if (!fechafinal.equals("")) {
                condicion = " AND año < " + fechafinal + "";
            }

            if (opcionesEstudio.equals("1")) {
                condicion += " AND estudio='PREGRADO' ";
            }
            if (opcionesEstudio.equals("2")) {
                condicion += " AND estudio='POSGRADO' ";
            }

            if (opcionesBusqueda.equals("1")) {
                return proyectoFachada.buscarTitulo(busqueda, condicion);
            } else if (opcionesBusqueda.equals("3")) {
                return proyectoFachada.buscarTituloPalabrasclaves(busqueda, condicion);
            } else {
                return proyectoFachada.buscarPalabrasclaves(busqueda, condicion);
            }
        }
    }

    public ProyectoFachada getProyectoFachada() {
        return proyectoFachada;
    }
}
