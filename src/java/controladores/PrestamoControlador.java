/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import entidades.Prestamo;
import entidades.Proyecto;
import entidades.Usuario;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.PrestamoFachada;
import modelo.ProyectoFachada;
import modelo.UsuarioFachada;

/**
 * Controla la información desde las paginas prestar.xhtml y
 * hacia ProyectoFachada para realizar la creacion de prestamos
 * proyectoFachada, proyecto, usuario, usuario, prestamoFachada, 
 * prestamo, codigoProyecto, cedulaUsuario, fechaDevolucion, fechaMinima
 * opcionesPrestamo
 *
 * @author Grupo DCJ
 * @version 20-02-2014
 */
@ManagedBean(name = "controladorPrestamo")
@SessionScoped
public class PrestamoControlador implements Serializable{

    @EJB
    private ProyectoFachada proyectoFachada;
    private Proyecto proyecto;
    @EJB
    private UsuarioFachada usuarioFachada;
    private Usuario usuario;
    @EJB
    private PrestamoFachada prestamoFachada;
    private Prestamo prestamo;
    private String codigoProyecto;
    private String cedulaUsuario;
    private Date fechaDevolucion;
    private Date fechaMinima;
    private String opcionesPrestamo;

    public PrestamoControlador() {
    }

    public ProyectoFachada getProyectoFachada() {
        return proyectoFachada;
    }

    public void setProyectoFachada(ProyectoFachada proyectoFachada) {
        this.proyectoFachada = proyectoFachada;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public UsuarioFachada getUsuarioFachada() {
        return usuarioFachada;
    }

    public void setUsuarioFachada(UsuarioFachada usuarioFachada) {
        this.usuarioFachada = usuarioFachada;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public PrestamoFachada getPrestamoFachada() {
        return prestamoFachada;
    }

    public void setPrestamoFachada(PrestamoFachada prestamoFachada) {
        this.prestamoFachada = prestamoFachada;
    }

    public Prestamo getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(Prestamo prestamo) {
        this.prestamo = prestamo;
    }

    public String getCodigoProyecto() {
        return codigoProyecto;
    }

    /**
     * setCodigoProyecto
     * permite actualizar la informacion del proyecto prestado,
     * ayuda a la organizacion en la vista
     * @param codigoProyecto 
     */
    public void setCodigoProyecto(String codigoProyecto) {
        this.codigoProyecto = codigoProyecto;
        buscarProyecto();
    }

    public String getCedulaUsuario() {
        return cedulaUsuario;
    }
    
      /**
     * setCedulaUsuario
     * permite actualizar la informacion del usuario prestado,
     * ayuda a la organizacion en la vista
     * @param cedulaUsuario 
     */
    public void setCedulaUsuario(String cedulaUsuario) {
        this.cedulaUsuario = cedulaUsuario;
        buscarUsuario();
    }

    public String getOpcionesPrestamo() {
        return opcionesPrestamo;
    }

    public void setOpcionesPrestamo(String opcionesPrestamo) {
        this.opcionesPrestamo = opcionesPrestamo;
    }

    public Date getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public Date getFechaMinima() {
        return fechaMinima;
    }
    
    /**
     * prestar
     * permite realizar la operacion de prestamo en el sistema
     * @PRE: para que el registro sea exitoso, Es necesario que proyecto, 
     * usuario, fecha entrega, sean diferentes de nulos y que el proyecto 
     * esté disponible en el sistema
     * @POS: gestiona la informacion para realizar el registro del prestamo
     * si no es posible realizar el prestamo manda un mensaje a la pagina prestar.xhtml
     */
    public void prestar() {
        
        FacesMessage msg = null;
        String articulos="";
        if(opcionesPrestamo.equals("1"))
        {
            articulos="documentos";
        }
        if(opcionesPrestamo.equals("2"))
        {
            articulos="cd";
        }
        if(opcionesPrestamo.equals("3"))
        {
            articulos="documentos,cd";
        }
       
        
        if (proyecto != null) {
            if (proyecto.getEstado().equals("DISPONIBLE")) {
                if (usuario != null) {                   
                    prestamo=new Prestamo();
                    prestamo.setProyecto(proyecto);
                    prestamo.setUsuario(usuario);
                    prestamo.setFechaPrestamo(new Date());
                    prestamo.setFechaDevolucion(fechaDevolucion);
                    prestamo.setArticulos(articulos);
                    prestamo.setEstado("ACTIVIO");  
                    prestamoFachada.adicionar(prestamo);
                    proyecto.setEstado("PRESTADO");
                    proyectoFachada.actualizar(proyecto);
                    inicioOpciones();
                    msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Préstamo Realizado", "" );
                    
                } else {
                     msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Usuario No Encontrado", "");
                }
            } else {
                msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Proyecto No Disponible", "");
            }
        } else {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Proyecto No Encontrado", "");
        }
         FacesContext.getCurrentInstance().addMessage(null, msg);  
    }

    public void buscarUsuario() {
        usuario = usuarioFachada.buscarCedula(cedulaUsuario);
    }

    public void buscarProyecto() {
        proyecto = proyectoFachada.buscarProyectoCodigo(codigoProyecto);
    }

    /**
     * parametriza los valores de los atributos con el fin de parametrizar el
     * prestamo del usuario
     *
     * @return void
     */
    public void inicioOpciones() {
        opcionesPrestamo = "1";
        codigoProyecto = "";
        cedulaUsuario = "";
        prestamo = null;
        usuario = null;
        proyecto = null;
        fechaDevolucion = null;
        fechaMinima = new Date();

    }
}
