/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * Controla la sesion de usuario del sistema
 * nombre, clave
 *
 * @author Grupo DCJ
 * @version 20-02-2014
 */

@ManagedBean(name = "controladorUsuarioSesion")
@SessionScoped
public class UsuarioSesionControlador implements Serializable{
    
    private static final long serialVersionUID = -2152389656664659476L;
    private String nombre;
    private String clave;
   

    /**
     * Constructor
     */
    public UsuarioSesionControlador() {
    }

   

    /**
     * salir
     * permite finalizar la sesion del usuario en el sistema
     */
    public void salir() {
        FacesMessage msg = null;
         msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cerrando Sesión De Usuario", "");
          FacesContext.getCurrentInstance().addMessage(null, msg);  
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        session.invalidate();        
    }
    
    
    /**
     * parametriza los valores de los atributos con el fin de parametrizar la
     * sesion de usuario
     *
     */
    public void inicioOpciones() {
       nombre="";
       clave="";
    }
    
}
