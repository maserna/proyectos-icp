/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * contiene información de los usuarios que solicita un prestamo contiene
 * cedula, nombre, correo,telefono, direccion
 *
 * @author Grupo DCJ
 * @version 1.0 27-03-2014
 */
@Entity
@SessionScoped
@XmlRootElement
public class Usuario implements Serializable {

    @Id
    @Column(columnDefinition = "VARCHAR(10)", name = "CEDULA")
    private String cedula;
    @Column(columnDefinition = "VARCHAR(50)", name = "NOMBRE")
    private String nombre;
    @Column(columnDefinition = "VARCHAR(50)", name = "CORREO")
    private String correo;
    @Column(columnDefinition = "VARCHAR(15)", name = "TELEFONO")
    private String telefono;
    @Column(columnDefinition = "VARCHAR(30)", name = "DIRECCION")
    private String direccion;

    /**
     * Constructor
     */
    public Usuario() {
    }

    /**
     * getCedula
     *
     * @return cedula-- cedula del usuario, tambien es el identificador del
     * objeto
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * setCedula
     *
     * @param cedula -- cedula del usuario
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * getNombre
     *
     * @return nombre -- nombre del usuario, tambien es el identificador del
     * objeto
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * setNombre
     *
     * @param nombre -- nombre del usuario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * getCorreo
     *
     * @return correo -- correo electronico del usuario
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * setCorreo
     *
     * @param correo -- correo electronico del usuario
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * getTelefono
     *
     * @return telefono -- telefono del usuario
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * setTelefono
     *
     * @param telefono -- telefono del usuario
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * getDireccion
     *
     * @return direccion -- direccion del usuario
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * setDireccion
     *
     * @param direccion -- direccion del usuario
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Devuelve el hash del objeto basado en el identificador "la cedula" del
     * usuario
     *
     * @return int codigo hash
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.cedula != null ? this.cedula.hashCode() : 0);
        return hash;
    }

    /**
     * dos usuarios son iguales si tienen el mismo identificador "la cedula"
     *
     * @param obj Objeto a comparar
     * @return true si el objeto es un usuario con el mismo identificador,
     * cedula, retorna false en caso contrario
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if ((this.cedula == null) ? (other.cedula != null) : !this.cedula.equals(other.cedula)) {
            return false;
        }
        return true;
    }
}
