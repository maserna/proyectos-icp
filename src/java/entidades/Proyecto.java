/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * contiene información de los proyectos que contiene id, año, autores,
 * asesores, grupos de investigación titulo, cd, documentos, resumen, ubicación,
 * semestre, estudio está relacionada con autores, asesores, grupos de
 * investigacion, palabras claves y estado del proyecto
 *
 * @author Grupo DCJ
 * @versio 1.0 20-02-2014
 */
@Table
@Entity
@XmlRootElement
public class Proyecto implements Serializable {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", columnDefinition = "VARCHAR(20)")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    /*
     * Establece una unión entre el id de un proyecto y el PROYECTOCOLLECTION_ID establecido como llave foranea dentro de la tabla 
     * PROYECTO_PALABRACLAVE para devolver el o los campos de PALABRASCLAVES_PALABRA
     */
    @JoinTable(name = "PROYECTO_PALABRACLAVE", joinColumns = {
        @JoinColumn(name = "PROYECTOCOLLECTION_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "PALABRASCLAVES_PALABRA", referencedColumnName = "PALABRA")})
    @ManyToMany
    private Collection<PalabraClave> palabrasClaves;
    private Short año;
    /*
     * Establece una unión entre el id de un proyecto y el PROYECTO_ID establecido como llave foranea dentro de la tabla 
     * PROYECTO_AUTOR para devolver el o los campos de AUTORES_ID
     */
    @JoinTable(name = "PROYECTO_AUTOR", joinColumns = {
        @JoinColumn(name = "PROYECTO_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "AUTORES_ID", referencedColumnName = "ID")})
    @OneToMany
    private Collection<Autor> autores;
    /*
     * Establece una unión el id de un proyecto y el PROYECTO_ID establecido como llave foranea dentro de la tabla 
     * PROYECTO_ASESOR para devolver el o los campos de ASESORE_ID
     */
    @JoinTable(name = "PROYECTO_ASESOR", joinColumns = {
        @JoinColumn(name = "PROYECTO_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "ASESORE_ID", referencedColumnName = "ID")})
    @OneToMany
    private Collection<Asesor> asesore;
    /*
     * relaciona el id de un proyecto el proyecto_id establecido como llave foranea dentro de la tabla 
     * PROYECTO_GRUPOSINVESTIGACION para devolver el o los campos de GRUPOSINVESTIGACION_NOMBRE
     */
    @JoinTable(name = "PROYECTO_GRUPOSINVESTIGACION", joinColumns = {
        @JoinColumn(name = "PROYECTO_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "GRUPOSINVESTIGACION_NOMBRE", referencedColumnName = "NOMBRE")})
    @OneToMany
    private Collection<GruposInvestigacion> gruposInvestigacion;
    private String titulo;
    @Column(columnDefinition = "VARCHAR(2)")
    private String cd;
    @Column(columnDefinition = "VARCHAR(2)")
    private String documentos;
    @Column(columnDefinition = "VARCHAR(1200)")
    private String resumen;
    @Column(columnDefinition = "VARCHAR(500)")
    private String ubicacion;
    @Column(columnDefinition = "VARCHAR(8)")
    private String semestre;
    @Column(columnDefinition = "VARCHAR(9)")
    private String estudio;
    @Column(columnDefinition = "VARCHAR(10)")
    private String estado;

    /**
     * Constructor
     */
    public Proyecto() {
    }

    /**
     * getId
     *
     * @return id -- identificador del objeto
     */
    public String getId() {
        return id;
    }

    /**
     * getAño
     *
     * @return año -- Año de publicación del proyecto
     */
    public Short getAño() {
        return año;
    }

    /**
     * getAutores
     *
     * @return autores - coleccion de aurores del proyecto
     */
    public Collection<Autor> getAutores() {
        return autores;
    }

    /**
     * getAsesore
     *
     * @return asesore - coleccion de asesores del proyecto
     */
    public Collection<Asesor> getAsesore() {
        return asesore;
    }

    /**
     * getGrupoInvestigacion
     *
     * @return gruposInvestigacion - coleccion de grupos de Iivestigacion
     * Asociados del proyecto
     */
    public Collection<GruposInvestigacion> getGrupoInvestigacion() {
        return gruposInvestigacion;
    }

    /**
     * getTitulo
     *
     * @return titulo - titulo del proyecto
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * getResumen
     *
     * @return resumen - resumen del proyecto
     */
    public String getResumen() {
        return resumen;
    }

    /**
     * getUbicacion
     *
     * @return ubicacion - Descripción de la ubicación del proyecto
     */
    public String getUbicacion() {
        return ubicacion;
    }

    /**
     * setId
     *
     * @param id -- identificador del objeto
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * setAño
     *
     * @param año -- Año de publicación del proyecto
     */
    public void setAño(Short año) {
        this.año = año;
    }

    /**
     * setAutores
     *
     * @param autores -nueva coleccion de autores
     */
    public void setAutores(Collection<Autor> autores) {
        this.autores = autores;
    }

    /**
     * setAsesore
     *
     * @param asesore -nueva coleccion de asesores
     */
    public void setAsesore(Collection<Asesor> asesore) {
        this.asesore = asesore;
    }

    /**
     * setGruposInvestigacion
     *
     * @param gruposInvestigacion -nueva coleccion de grupos Investigacion
     * Asociada al proyecto
     */
    public void setGruposInvestigacion(Collection<GruposInvestigacion> gruposInvestigacion) {
        this.gruposInvestigacion = gruposInvestigacion;
    }

    /**
     * setTitulo
     *
     * @param titulo -nuevo titulo del proyecto
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * setResumen
     *
     * @param resumen -nuevo resumen del proyecto
     */
    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    /**
     * setUbicacion
     *
     * @param ubicacion -nueva ubicacion del proyecto
     */
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    /**
     * getCd
     *
     * @return cd -si el proyecto tiene material de cd
     */
    public String getCd() {
        return cd;
    }

    /**
     * setCd
     *
     * @param cd -Cambio en el elemento de cd del proyecto
     */
    public void setCd(String cd) {
        this.cd = cd;
    }

    /**
     * getDocumentos
     *
     * @return documentos -si el proyecto tiene material de documentos
     */
    public String getDocumentos() {
        return documentos;
    }

    /**
     * setDocumentos
     *
     * @param documentos -Cambio en el elemento de documentos del proyecto
     */
    public void setDocumentos(String documentos) {
        this.documentos = documentos;
    }

    /**
     * getSemestre
     *
     * @return semestre -Indica el periodo académico en el cual se desarrolló el
     * proyecto Primero, segundo
     */
    public String getSemestre() {
        return semestre;
    }

    /**
     * setSemestre
     *
     * @param semestre -cambio en el periodo académico en el cual se desarrolló
     * el proyecto Primero, segundo
     */
    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    /**
     * getEstudio
     *
     * @return estudio -Indica el tipo de estudio del proyecto PREGRADO,
     * POSGRADO
     */
    public String getEstudio() {
        return estudio;
    }

    /**
     * setEstudio
     *
     * @param estudio -cambio en el tipo de estudio del proyecto PREGRADO,
     * POSGRADO
     */
    public void setEstudio(String estudio) {
        this.estudio = estudio;
    }

    /**
     * getEstado
     *
     * @return estado -Indica Disponibilidad del proyecto DISPONIBLE, PRESTADO
     */
    public String getEstado() {
        return estado;
    }

    /**
     * setEstado
     *
     * @param estado -CAMBIO en la Disponibilidad del proyecto DISPONIBLE,
     * PRESTADO
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * getPalabrasClaves
     *
     * @return estado -palabras Claves Asociada al proyecto
     */
    @XmlTransient
    public Collection<PalabraClave> getPalabrasClaves() {
        return palabrasClaves;
    }

    /**
     * setPalabrasClaves
     *
     * @param palabrasClaves -palabras Claves Asociada al proyecto
     */
    public void setPalabrasClaves(Collection<PalabraClave> palabrasClaves) {
        this.palabrasClaves = palabrasClaves;
    }

    /**
     * Devuelve el hash del objeto basado en el titulo del proyecto
     *
     * @return codigo hash
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.titulo != null ? this.titulo.hashCode() : 0);

        return hash;
    }

    /**
     * Dos proyectos son iguales si tienen el mismo identificador
     *
     * @param obj Objeto a comparar
     * @return true si el objeto es un proyecto con el mismo id, false de lo
     * contrario
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Proyecto other = (Proyecto) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    /*
     * palabrasClaves
     * Recorre la lista que contiene los tags relacionados 
     * a un proyecto obtiene la palabra clave y la 
     * va concatenando en una cadena 
     * @pre listaTags !=null
     * @return String cadenaTags retorna una cadena que contiene los tags
     * asociados a un proyecto separados por un caracter ","   
     */
    public String palabrasClaves() {
        String r = "";
        List re = (List) palabrasClaves;
        if (re != null) {
            for (int i = 0; i < re.size(); i++) {
                PalabraClave t = (PalabraClave) re.get(i);
                r += t.getPalabra() + ", ";
            }
        }
        return r;
    }

    /*
     * Recorre la lista que contiene los autores relacionados 
     * a un proyecto obtiene el nombre del autor y lo 
     * va concatenando en una cadena 
     * @pre listaAutores !=null
     * @return String cadenaAutores retorna una cadena que contiene los nombres de los autores
     * asociados a un proyecto separados por un caracter ","
     * 
     */
    public String autoresColection() {
        String r = "";
        List re = (List) autores;
        if (re != null) {
            for (int i = 0; i < re.size(); i++) {
                Autor p = (Autor) re.get(i);
                r += p.getPersona().getNombre() + ", ";
            }
        }
        return r;
    }

    /*
     * Recorre la lista que contiene los asesores relacionados 
     * a un proyecto obtiene el nombre del asesor y lo 
     * va concatenando en una cadena 
     * @pre listaAsesores !=null
     * @return String cadenaAsesores retorna una cadena que contiene los nombres de los asesores
     * asociados a un proyecto separados por un caracter ","
     * 
     */
    public String asesoreColection() {
        String r = "";
        List re = (List) asesore;
        if (re != null) {
            for (int i = 0; i < re.size(); i++) {
                Asesor p = (Asesor) re.get(i);
                r += p.getPersona().getNombre() + ", ";
            }
        }
        return r;
    }

    /*
     * Recorre la lista que contiene los grupos de investigacion asociados
     * a un proyecto obtiene el nombre del grupo de investigación y lo 
     * va concatenando en una cadena 
     * @pre listaGruposInvestigacion !=null
     * @return String cadenaGruposInvestigacion retorna una cadena que contiene los grupos de investigación 
     * separadados por un caracter ","
     * 
     */
    public String grupoInvestigacionColection() {
        String r = "";
        List re = (List) gruposInvestigacion;
        if (re != null) {
            for (int i = 0; i < re.size(); i++) {
                GruposInvestigacion g = (GruposInvestigacion) re.get(i);
                r += g.getNombre() + ", ";
            }
        }
        return r;
    }
}