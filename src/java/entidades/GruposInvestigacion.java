/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.faces.bean.SessionScoped;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Tiene la información de los grupos de investigación incluyendo su nombre
 *
 * @author Grupo DCJ
 * @version 1.0 20-02-2014
 *
 */
@Entity
@SessionScoped
@XmlRootElement
public class GruposInvestigacion implements Serializable {

    @Id
    private String Nombre;

    /**
     * Constructor
     */
    public GruposInvestigacion() {
    }

    /**
     * getNombre
     *
     * @return nombre - nombre del objeto
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * SetNombre
     *
     * @param Nombre -- nombre del objeto
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * Devuelve el hash del objeto basado en el identificador del grupo de
     * investigación
     *
     * @return int codigo hash
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.Nombre != null ? this.Nombre.hashCode() : 0);
        return hash;
    }

    /**
     * si dos grupos de investigación son iguales si tienen el mismo
     * identificador
     *
     * @param obj Objeto a comparar
     * @return true si el objeto es un grupo de investigación con el mismo
     * nombre, retorna false en caso contrario
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GruposInvestigacion other = (GruposInvestigacion) obj;
        if ((this.Nombre == null) ? (other.Nombre != null) : !this.Nombre.equals(other.Nombre)) {
            return false;
        }
        return true;
    }

    /**
     * Representa el nombre del grupo de investigación en forma de cadena
     *
     * @return String una cadena que incluye el nombre del grupo de
     * investigación
     */
    @Override
    public String toString() {
        return "GruposInvestigacion{" + "Nombre=" + Nombre + '}';
    }
}
