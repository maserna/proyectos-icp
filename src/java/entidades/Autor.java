/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.persistence.OneToOne;

/**
 * Contiena la informacion de los autores incluyendo un id autonumerico,
 * Persona,
 *
 * @author Grupo DCJ
 * @version 1.0 27-03-2014
 */
@Table
@Entity
@XmlRootElement
public class Autor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @OneToOne
    private Persona persona;

    /**
     * Constructor
     */
    public Autor() {
    }

    /**
     * getId
     *
     * @return id -- identificador del objeto
     */
    public int getId() {
        return id;
    }

    /**
     * setId
     *
     * @param id -- identificador del objeto
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * getPersona
     *
     * @return persona - Asociada al objeto
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * setPersona
     *
     * @param persona -Asociada al objeto
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * Devuelve el hash del objeto basado en el identificador del autor
     *
     * @return int codigo hash
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.id;
        return hash;
    }

    /**
     * dos aurores son iguales si tienen el mismo identificador
     *
     * @param obj Objeto a comparar
     * @return true si el objeto es un autor con el mismo identificador, id,
     * retorna false en caso contrario
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Autor other = (Autor) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
