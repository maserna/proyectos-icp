package entidades;

import java.io.Serializable;
import javax.faces.bean.SessionScoped;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contiena la informacion de las personas incluyendo un id autonumerico,
 * nombre,
 *
 * @author Grupo DCJ
 * @version 1.0 20-02-2014
 */
@Entity
@SessionScoped
@XmlRootElement
public class Persona implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String nombre;

    /**
     * Constructor
     */
    public Persona() {
    }

    /**
     * getId
     *
     * @return id -- identificador del objeto
     */
    public int getId() {
        return id;
    }

    /**
     * getNombre
     *
     * @return nombre -- nombre del objeto
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * setNombre
     *
     * @param nombre -- nombre del objeto
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * setId
     *
     * @param id -- identificador del objeto
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Devuelve el hash del objeto basado en el identificador de persona
     *
     * @return int codigo hash
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.id;
        return hash;
    }

    /**
     * si dos personas son iguales si tienen el mismo identificador
     *
     * @param obj Objeto a comparar
     * @return true si el objeto es una persona con el mismo id, en caso
     * contrario retorna false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    /**
     * Representa el nombre del persona en forma de cadena
     *
     * @return String una cadena que incluye el nombre de la persona
     */
    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + '}';
    }
}
