/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * contiene información de prestamo de los proyectos que contiene id, usuario,
 * proyecto, estado, fechaPrestamo, fechaDevolucion, articulos
 *
 *
 * @author Grupo DCJ
 * @versio 1.0 27-03-2014
 */
@Entity
@SessionScoped
@XmlRootElement
public class Prestamo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToOne
    @JoinColumn(name = "USUARIO_CEDULA")
    private Usuario usuario;
    @OneToOne
    @JoinColumn(name = "PROYECTO_ID")
    private Proyecto proyecto;
    @Column(columnDefinition = "VARCHAR(10)", name = "ESTADO")
    private String estado;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaPrestamo;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaDevolucion;
    @Column(columnDefinition = "VARCHAR(13)", name = "ARTICULOS")
    private String articulos;

    /**
     * Constructor
     */
    public Prestamo() {
    }

    /**
     * getId
     *
     * @return id -- identificador del objeto
     */
    public long getId() {
        return id;
    }

    /**
     * setId
     *
     * @param id -- identificador del objeto
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * getUsuario
     *
     * @return usuario -- objeto del usuario Solicitante del préstamo
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * setUsuario
     *
     * @param usuario -- objeto del usuario Solicitante del préstamo
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * getProyecto
     *
     * @return proyecto -- proyecto Solicitado del préstamo
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * setProyecto
     *
     * @param proyecto -- nuevo proyecto Solicitado del préstamo
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * getEstado
     *
     * @return estado -- estado actual del préstamo ACTIVO, INACTIVO
     */
    public String getEstado() {
        return estado;
    }

    /**
     * setProyecto
     *
     * @param proyecto -- CAMBIA EL estado actual del préstamo ACTIVO, INACTIVO
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * getFechaPrestamo
     *
     * @return fechaPrestamo -- Fecha en que se realizó el préstamo
     */
    public Date getFechaPrestamo() {
        return fechaPrestamo;
    }

    /**
     * setFechaPrestamo
     *
     * @param fechaPrestamo -- cambio Fecha en que se realizó el préstamo
     */
    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    /**
     * getFechaDevolucion
     *
     * @return fechaDevolucion -- Fecha máxima de devolución del préstamo
     */
    public Date getFechaDevolucion() {
        return fechaDevolucion;
    }

    /**
     * setFechaDevolucion
     *
     * @param fechaDevolucion -- cambio Fecha máxima de devolución del préstamo
     */
    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    /**
     * getArticulos
     *
     * @return articulos -- Lista de artículos solicitados en el préstamo
     */
    public String getArticulos() {
        return articulos;
    }

    /**
     * setArticulos
     *
     * @param articulos -- cambio en la Lista de artículos solicitados en el
     * préstamo
     */
    public void setArticulos(String articulos) {
        this.articulos = articulos;
    }

    /**
     * Devuelve el hash del objeto basado en el id del prestamo
     *
     * @return codigo hash
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    /**
     * Dos prestamos son iguales si tienen el mismo identificador
     *
     * @param obj Objeto a comparar
     * @return true si el objeto es un prestamo con el mismo id, false de lo
     * contrario
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Prestamo other = (Prestamo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
