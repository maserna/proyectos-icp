/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.faces.bean.SessionScoped;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Tiene la información de los grupos de investigación incluyendo su nombre
 *
 * @author Grupo DCJ
 * @version 1.0 20-02-2014
 *
 */
@Table
@Entity
@SessionScoped
@XmlRootElement
public class PalabraClave implements Serializable {

    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "PALABRA")
    private String palabra;
    @ManyToMany(mappedBy = "palabrasClaves")
    private Collection<Proyecto> proyectoCollection;

    /**
     * Constructor
     */
    public PalabraClave() {
    }

    /**
     * getPalabra
     *
     * @return palabra -- palabra del objeto
     */
    public String getPalabra() {
        return palabra;
    }

    /**
     * setPalabra
     *
     * @param palabra -- palabra del objeto
     */
    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    /**
     * getProyectoCollection
     * @return proyectoCollection - coleccion de proyectos
     */
    @XmlTransient
    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    /**
     * setProyectoCollection
     * @param proyectoCollection -nueva coleccion de proyectos
     */
    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    /**
     * Devuelve el hash del objeto basado en la palabra de palabrasClaves
     *
     * @return int codigo hash
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.palabra != null ? this.palabra.hashCode() : 0);
        return hash;
    }

    /**
     * dos palabras claves son iguales si tienen la misma palabra
     *
     * @param obj Objeto a comparar
     * @return true si el objeto es una palabra igual retorna false en caso
     * contrario
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PalabraClave other = (PalabraClave) obj;
        if ((this.palabra == null) ? (other.palabra != null) : !this.palabra.equals(other.palabra)) {
            return false;
        }
        return true;
    }

    /**
     * Representa la palabra clave
     *
     * @return String una cadena que incluye la palabra del objeto
     */
    @Override
    public String toString() {
        return "PalabraClave{" + "palabra=" + palabra + '}';
    }
}
