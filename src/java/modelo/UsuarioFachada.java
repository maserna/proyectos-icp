/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import entidades.Usuario;
import javax.ejb.Stateless;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Contiene información relacionada con la persistencia permintiendo la
 * ejecución de consultas hacia la base de datos
 *
 * @author Grupo DCJ
 * @version 1.0 27-07-2014
 *
 */
@ViewScoped
@Stateless
public class UsuarioFachada {
    
     /**
     * Manejador de Persistencia
     */
    @PersistenceContext(unitName = "ProyectoSICP.V1.0PU")
    private EntityManager em;

   
    
     /*
     * permite buscar un usuario a través del criterio cedula
     * @param cedula- cedula del usuario
     * 
     * @return Usuario -- usuario con la informacion de las coincidencias de busqueda
     * @post el sistema recibe un usuario que contiene la información relevante a las coincidencias por criterios
     *       y debe se retornada para mostrar en pantalla
     */
    public Usuario buscarCedula(String cedula) {
        Query query2 = em.createNativeQuery("SELECT * FROM Usuario  WHERE CEDULA='"+cedula+"'",Usuario.class);  
        try{
             return (Usuario)query2.getSingleResult();        
        }
        catch(Exception e)
        {
            return null;
        }
       
    }
    
}
