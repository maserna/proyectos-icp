/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import entidades.Prestamo;
import javax.ejb.Stateless;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Contiene información relacionada con la persistencia permintiendo la
 * ejecución de consultas hacia la base de datos
 *
 * @author Grupo DCJ
 * @version 1.0 27-07-2014
 *
 */
@ViewScoped
@Stateless
public class PrestamoFachada {

    /**
     * Manejador de Persistencia
     */
    @PersistenceContext(unitName = "ProyectoSICP.V1.0PU")
    private EntityManager em;

    /*
     * permite adicionar un prestamo a al base de datos
     * @param prestamo --  prestamo para realizr el registro en la base de datos    
     * @post la base de datos recibe un nuevo registro de prestamo
     */
    public void adicionar(Prestamo prestamo) {
        em.persist(prestamo);
    }

    /*
     * permite actualizar un prestamo a al base de datos
     * @param prestamo --  prestamo para realizr el update en la base de datos     * 
     * @post la base de datos actualiza un registro de prestamo
     */
    public void actualizar(Prestamo prestamo) {
        em.merge(prestamo);
    }
}
