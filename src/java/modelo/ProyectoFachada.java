/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import entidades.Proyecto;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Contiene información relacionada con la persistencia permintiendo la
 * ejecución de consultas hacia la base de datos
 *
 * @author Grupo DCJ
 * @version 1.0 20-02-2014
 *
 */
@ViewScoped
@Stateless
public class ProyectoFachada {

    /**
     * Manejador de Persistencia
     */
    @PersistenceContext(unitName = "ProyectoSICP.V1.0PU")
    private EntityManager em;

    /*
     * permite buscar un proyecto a través del criterio titulo y palabras clave
     * @param String  nombre titulo del proyecto
     * @param String  condicion 
     * @return Collection<Proyecto> colección que almacena las coincidencias de proyectos con titulo y tags
     * @post el sistema recibe una coleccion que contiene la información relevante a las coincidencias por criterios
     *       y debe se retornada para mostrar en pantalla
     */
    public Collection<Proyecto> buscarTituloPalabrasclaves(String nombre, String condicion) {
        Query query2 = em.createNativeQuery("SELECT DISTINCT * FROM Proyecto  WHERE titulo LIKE '%" + nombre + "%'" + condicion
                + " union "
                + "(select * from PROYECTO where id in (SELECT  proyectocollection_id FROM Proyecto_palabraclave  WHERE  palabrasclaves_palabra LIKE '%"
                + nombre + "%') " + condicion + " ) ", Proyecto.class);
        return query2.getResultList();
    }

    /*
     * permite buscar un proyecto a través del criterio título
     * @param String  nombre titulo del proyecto
     * @param String  condicion 
     * @return Collection<Proyecto> colección que almacena las coincidencias de proyectos con un título 
     * @post el sistema recibe una coleccion que contiene la información relevante a las coincidencias por criterios
     *       y debe se retornada para mostrar en pantalla
     */
    public Collection<Proyecto> buscarTitulo(String nombre, String condicion) {
        Query query2 = em.createNativeQuery("SELECT * FROM PROYECTO  WHERE titulo LIKE '%" + nombre + "%'"
                + condicion, Proyecto.class);
        return query2.getResultList();
    }

    /*
     * permite buscar un proyecto a través del criterio solo con palabras clave
     * @param String  nombre titulo del proyecto
     * @param String  condicion 
     * @return Collection<Proyecto> colección que almacena las coincidencias de proyectos con tags
     * @post el sistema recibe una coleccion que contiene la información relevante a las coincidencias por criterios
     *       y debe se retornada para mostrar en pantalla
     */
    public Collection<Proyecto> buscarPalabrasclaves(String nombre, String condicion) {
        Query query2 = em.createNativeQuery("(select * from PROYECTO where id in (SELECT  proyectocollection_id FROM Proyecto_palabraclave  WHERE  palabrasclaves_palabra LIKE '%"
                + nombre + "%') " + condicion + " ) ", Proyecto.class);
        return query2.getResultList();
    }

    /*
     * permite buscar un proyecto a través del criterio codigo
     * @param String  codigo codigo del proyecto
     * 
     * @return Collection<Proyecto> colección que almacena las coincidencias de proyectos con un codigo 
     * @post el sistema recibe una coleccion que contiene la información relevante a las coincidencias por criterios
     *       y debe se retornada para mostrar en pantalla
     */
    public Collection<Proyecto> buscarCodigo(String codigo) {
        Query query2 = em.createNativeQuery("SELECT * FROM Proyecto  WHERE id='" + codigo + "'", Proyecto.class);
        return query2.getResultList();
    }

    /*
     * permite buscar un proyecto a través del criterio codigo
     * @param String -- codigo codigo del proyecto
     * 
     * @return Proyecto -- que cumpla las coincidencias de proyectos con un codigo 
     * @post el sistema recibe un proyecto que contiene la información relevante a las coincidencias por criterios
     *       y debe se retornada para mostrar en pantalla
     */
    public Proyecto buscarProyectoCodigo(String codigo) {
        Query query2 = em.createNativeQuery("SELECT * FROM Proyecto  WHERE id='" + codigo + "'", Proyecto.class);
        try {
            return (Proyecto) query2.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public void actualizar(Proyecto proyecto) {
        em.merge(proyecto);
    }
}
